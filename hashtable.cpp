// Required

#ifndef HASHTABLE_CPP
#define HASHTABLE_CPP

static const unsigned int default_capacity = 11;
static const char salt[] = "$1$########";

namespace cop4530
{
	// zero-argument constructor. Initializes the vector to a default capacity of
	// 11.  We use a prime number to enhance the quality of the hashing performed
	// later.  We shall refer to the resulting capacity as 11 buckets.
	HashTable::HashTable()
	{
		bucketVector.resize(default_capacity);
	}

	// 1-parameter constructor.  Initializes the vector to a specified capacity.
	// This constructor is called by the client to set the capacity of the
	// underlying vector to the highest prime number below n.
	HashTable::HashTable(size_t n)
	{
		bucketVector.resize(prime_below(n));
	}

	// Iterates through the vector and clears all the lists within each bucket.
	HashTable::~HashTable()
	{
		for (list < pair <string, string> > p : bucketVector)
		{
			p.clear();
		}
		bucketVector.clear();
	}

	const char * encrypted(string str)
	{
		char * password = new char [100];
		strcpy ( password, crypt(str.c_str(), salt));
    	string passStr = password;
    	return passStr.substr(passStr.find_last_of("$")+1).c_str();
	}

	bool HashTable::Add(string username, string password, bool encrypt)
	{
		list < pair <string, string> > & list = bucketVector[Index(username)];

		for (pair <string, string> p : list)
		{
			if (username.compare(p.first) == 0)
			{
				// Duplicate
				return false;
			}
		}

		pair <string, string> newPair = make_pair(username, encrypted(password));
		list.push_back(newPair);
		return true;
	}

	bool HashTable::Remove(string username)
	{
		list < pair <string, string> > & list = bucketVector[Index(username)];
		for (pair <string, string> p : list)
		{
			if (username.compare(p.first) == 0)
			{
				// found
				list.remove(p);
				return true;
			}
		}
		return false;
	}

	bool HashTable::Change(string username, string password)
	{
		string newPass;
		cout << "Enter new password: ";
		cin >> newPass;

		list < pair <string, string> > & list = bucketVector[Index(username)];
		for (pair <string, string> p : list)
		{
			if (username.compare(p.first) == 0)
			{
				string enPass = encrypted(password);
				if (enPass.compare(p.second) == 0)
				{
					p.second = encrypted(newPass);
					cout << endl;
					return true;
				}
			}
		}
		cout << endl;
		return false;
	}

	bool HashTable::Find(string username, string password)
	{
  		list < pair <string, string> > & list = bucketVector[Index(username)];
  		for (pair <string, string> p : list)
  		{
    			if (username.compare(p.first) == 0)
    			{
    				string enPass = encrypted(password);
    				if (enPass.compare(p.second) == 0)
      				{
      					return true;
      				}
    			}
  		}
  		return false;
	}

	bool HashTable::LoadFile(string filename)
	{
		ifstream f(filename);
		if (f)
		{
			while(!f.eof())
			{
				string line, username, password;
				getline(f, line);
				stringstream ss(line);
				getline(ss, username,':');
				getline(ss, password,' ');

				(*this).Add(username, password, false);
			}
			return true;
		}
		else
		{
			cout << "*****Error: Unable to open file " << filename << endl;
			return false;
		}
	}

	bool HashTable::WriteFile(string filename)
	{
		ofstream f(filename);

		for (list < pair <string, string> > l : bucketVector)
		{
			for (pair <string, string> p : l)
			{
				f << p.first << ':' << p.second << endl;
			}
		}

		f.close();
		return true;
	}

	void HashTable::Dump()
	{
		int index = 0;
		for (list < pair <string, string> > l : bucketVector)
		{
			cout << "v[" << index++ << "]: ";
			for (pair <string, string> p : l)
			{
				cout << setw(32) << left << p.first + ':' + p.second;
			}
			cout << endl;
		}
	}

	int HashTable::Size()
	{
		int count = 0;
		for (list < pair <string, string> > l : bucketVector)
		{
			for (pair <string, string> p : l)
			{
				count++;
			}
		}
		return count;
	}
	// Private

	//Returns the bucket index where the pair should be inserted
	unsigned int HashTable::Index (const string & key)
	{
		return hash_function(key) % bucketVector.capacity();
	}

	//Returns the hashed index
	unsigned int HashTable::hash_function (const string & s)
	{
		unsigned int i;
		unsigned long bigval = s[0];
		for (i = 1; i < s.size(); ++i)
			bigval = ((bigval & 65535) * 18000) + (bigval >> 16) + s[i];
		bigval = ((bigval & 65535) * 18000) + (bigval >> 16);
		return bigval & 65535;

	}

	// returns largest prime number <= n or zero if input is too large
	// This is likely to be more efficient than prime_above(), because
	// it only needs a vector of size n
	unsigned int HashTable::prime_below (unsigned int n)
	{
		if (n > max_prime)
		{
			std::cerr << "** input too large for prime_below()\n";
			return 0;
		}
		if (n == max_prime)
		{
			return max_prime;
		}
		if (n <= 1)
		{
			std::cerr << "** input too small\n";
			return 0;
		}

		// now: 2 <= n < max_prime
		vector <unsigned int> v (n + 1);
		setPrimes(v);
		while (n > 2)
		{
			if (v[n] == 1)
			{
				return n;
			}
			--n;
		}

		return 2;
	}

	//Sets all prime number indexes to 1. Called by method prime_below(n)
	void HashTable::setPrimes(vector<unsigned int>& vprimes)
	{
		int i = 0;
		int j = 0;

		vprimes[0] = 0;
		vprimes[1] = 0;
		int n = vprimes.capacity();

		for (i = 2; i < n; ++i)
		{
			vprimes[i] = 1;
		}

		for( i = 2; i*i < n; ++i)
		{
			if (vprimes[i] == 1)
			{
				for(j = i + i ; j < n; j += i)
				{
					vprimes[j] = 0;
				}
			}
		}
	}
}


#endif
