Hash-Tables-and-Its-Applications
================================

Project 5:  Hash Tables and Its Applications

Due: 04/18/2014

Educational Objectives:  Understand and get familiar with the data structure
hash tables, and its applications in managing user accounts.

Statement of Work: Implement a hash table ADT and other supporting user
interfaces for its application in managing user accounts and passwords.

Project Description:

In this assignment, you need to implement a program to simulate a simple
password server. The password server maintains user account information in a
hash table for fast user lookup. You are required to implement the hash table
(HashTable) ADT and the password server program.

Requirements of Password Server Program

The following are the basic requirements of the password server program, named
"pass.cpp."

The server must store username and encrypted password pairs in a HashTable
object.

The server must allow users to perform the following actions:

Load a password file into the HashTable object. The server reads the username
and encrypted password as a pair object from a file and adds the pair entry into
the HashTable object.

Add a new username and password. The server reads the username and password as a
pair object from the user (console) and adds the pair entry into the HashTable
object (note password should be encrypted before insertion).

Delete an existing user. The server reads the username from the user and removes
the corresponding pair entry from the HashTable object.

Change an existing user's password. The server reads in the username and
password as a pair object, the new password from the user and modifies the pair
objects' second entry in the HashTable object.

Check if a user exists. The server reads in the username and password as a pair
object from the user and checks if the pair entry exists in the HashTable object.

Show the structure and contents of the HashTable object to the screen.

Obtain the size of the HashTable (the number of username/password pairs in the
table).

Save the username and password combination into a file.

Exit the program.

The server program must re-prompt the user for the next choice from the menu and
exit the program only when the user selection the exit "x" option.

You must use the code provided in the pass.cpp file as a standardized option
menu to develop the implementation for the rest of the server program. You may
not alter the Menu() function.

Requirements of HashTable Class

A partial interface and implementation of the HashTable class is provided in the
hashtable.h and hashtable.cpp files. The underlying HashTable data structure is
a vector of lists, with each list storing pair objects. Hence, the underlying
data structure that HashTable uses to store data element would be declared as:

vector<list<pair<string,string>>> bucketVector;

The partially distributed HashTable files contains the following functionalities:

unsigned int Index (const string & str): Returns the index of the bucketVector where the string str is hashed into.  The parameter str stores the username.

unsigned int hash_function   (const string& str): Returns an integer value where the string str is hashed.  The parameter str stores the username.  This method is called by the Index method.

unsigned int prime_below(unsigned int n): Returns the highest prime number below n.  This method is called by the 1-parameter constructor.

void setPrimes(vector<unsigned int> &v): Evaluates the index value of a vector object and sets the corresponding element to store the value 1 if the index was a prime number and 0 if the index was not a prime number.  This method is called by the prime_below method.

Note: The vector object used in this method is used solely for the purpose of obtaining the highest prime number below n and is not the same vector object representing the HashTable.

In addition, your full implementation MUST AT LEAST provide the following functionality:

HashTable(): zero-argument constructor. Initializes the vector to a default capacity of 11.  We use a prime number to enhance the quality of the hashing performed later.  We shall refer to the resulting capacity as 11 buckets.

HashTable(size_t n): 1-parameter constructor.  Initializes the vector to a specified capacity.  This constructor is called by the client to set the capacity of the underlying vector to the highest prime number below n.

~HashTable():destructor. Iterates through the vector and clears all the lists within each bucket.

Method that loads entries from a file: This method should read the username and password entry from a file and add the entries into the HashTable.  Each pair entry is separated by a carriage return.  A line entry is represented in the format:

username:encrypted_password

For example, a sample entry file is as follows:



hemmi:I5ltky7/QQ.yHP1hhZrcy/

marge:qnR/8Wr64NvX3qQUy.Q7U1

hank:dJP.JOXf9U3VUdm8QunMy/

toh:Czz4fmL29pz0vLPvaCr1.0

sean:x.PthYV62/Iu9ecwrGKBV0

stuart:sN0AKSTpbrRfpePPk4QpI/

matthew:CjA3fXUld9/nilBzoK1pr0

kelly:RixHNHGrE6oUZda1O/lvZ.

Method that adds a user: This method should take the pair object containing the username and encrypted password and add the entry into the HashTable only if the username does not exist in the HashTable.

A password MUST be encrypted before storage.  For this project, we shall use the GNU C Library's crypt() method to encrypt the password.  The algorithm for the crypt() method shall be MD5-based.  The salt shall be the character stream "$1$########".   The resulting encrypted character stream is the

"$1$########" + ‘$' + 22 characters = 34 characters in total.

A user password is the sub string containing the last 22 characters, located after the 3rd ‘$'.

Note: A sample program to demonstrate the use of the crypt() method is also provided. In order compile a program calling crypt(), you may need to link with the crypt library. You can read more information on the manual page of crypt().

Method that removes a user: This method should find the username from the HashTable and remove the pair entry from the HashTable.

Method that changes a username's password: This method should find the pair object containing the username and encrypted password and modify the pair's second entry in the HashTable with the new password.

Method that find's a username: This method should inform the user if the username and encrypted password pair entry exists in the HashTable.

Method that prints HashTable's contents: This method should print the structure and contents of the HashTable to the screen.

Method that returns the size: This method should return the size of the HashTable.

Method that writes entries to a file: This method should write the username and password entries in the HashTable to a file.  Each entry is separated by a carriage return.  A line entry is represented in the format:

username:encrypted_password

For example, a sample entry output file is as follows:

hemmi:I5ltky7/QQ.yHP1hhZrcy/

marge:qnR/8Wr64NvX3qQUy.Q7U1

hank:dJP.JOXf9U3VUdm8QunMy/

toh:Czz4fmL29pz0vLPvaCr1.0

sean:x.PthYV62/Iu9ecwrGKBV0

stuart:sN0AKSTpbrRfpePPk4QpI/

matthew:CjA3fXUld9/nilBzoK1pr0

kelly:RixHNHGrE6oUZda1O/lvZ.

Extra-credit part I (10 points)

You may submit an alternative version to your program named spass.cpp, in which
the program hides the user's entries whenever the user keys in a password or new
password.

Extra-credit part II (10 - 20 points)

Develop a nice graphical user interface (GUI) for this project. Your GUI must
support at least the user interfaces supported by the plain-text version user
manual in the provided code. For this part, you can work on Windows or Mac (10
extra points). If you work on linprog.cs.fsu.edu, you have additional 10 points
(that is you will get 20 extra points) for GUI.

Provided Partial Code
The following partial code has been provided to you.

1.  hashtable.h : partial implementation

2.  hashtable.cpp : partial implementation

3.  pass.x : sample executable for linprog.cs.fsu.edu

4.  spass.x: sample executable with hidden password entries for linprog.cs.fsu.edu

5.  test1: sample test case (which contains the commands that a user will type. You can redirect it to pass.x as "pass.x < test1".

6.  scrypt.cpp: sample program to use crypt() to encrypt password.

7.  scrypt.x: executable code of scrypt.cpp.

Deliverables
1.  Your implementation must be entirely contained in the following files, which MUST be named in the same way.

a.  hashtable.h

b.  hashtable.cpp

c.  pass.cpp

d.  spass.cpp (optional: extra-credit part I)

d.  gpass.cpp (optional: extra-credit part II. For this one, you do not need to follow the given name)

e.   makefile

2. Submit all the files in a tar file. If you have implemented the extra-points functions, please indicate so when you submit your homework. Please also indicate if you implement the GUI on Windows, Mac, or linprog. If you implement the GUI on Windows or Mac, you need to set up a time with the TA to demo your code.

3.  Your program must compile on linprog.cs.fsu.edu. If you program does not compile on linprog, the grader cannot test your submission. Your executable(s) must be named pass.x and spass.x (for extra-credit option).

4.   The interaction and output (including error messages) of your client's executable(s) must behave in the same manner as the distributed pass.x and spass.x (on linprog.cs.fsu.edu). For example, one of the ways to test your program would be to run a "diff" command between the output file(s) created by your executable(s) and the output file(s) created by the distributed executable(s).

5. Note that, your GUI must work completely, we will not grade any partial implementation, for example, the code that cannot be compiled or cannot run. No partial credits are given for this part.

Points will be deducted for not complying with these requirements.
