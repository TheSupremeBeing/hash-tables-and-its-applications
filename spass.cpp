// Optional: Extra Credit
/* Details:
    You may submit an alternative version to your program named spass.cpp, in which
    the program hides the user's entries whenever the user keys in a password or new
    password.
*/

#include <iostream>
#include <string>
#include <utility>
#include <cstdlib>
#include "hashtable.h"

using namespace std;
using namespace cop4530;

void Menu()
{
  cout << "\n\n";
  cout << "l - Load From File" << endl;
	cout << "a - Add User" << endl;
	cout << "r - Remove User" << endl;
	cout << "f - Find User" << endl;
	cout << "d - Dump HashTable" << endl;
	cout << "s - HashTable Size" << endl;
	cout << "w - Write to Password File" << endl;
	cout << "x - Exit program" << endl;
	cout << "\nEnter choice : ";
}

void start()
{
  int size;
  cout << "Enter preferred hash table capacity: ";
  cin >> size;
  cout << endl;
  HashTable hTable(size);

  char c;
  Menu();
  cin >> c;
  cout << endl;

  string filename, username, password;
  while (c != 'x')
  {
		if (c == 'l')
		{
      // Load From File
      cout << "Enter filename to load from: ";
      cin >> filename;

      // Function to load from file
      if (hTable.LoadFile(filename))
      {
        cout << "Load Successful." << endl;
      }
      else
      {
        cout << "Error in loading file." << endl;
      }
    }
    else if (c == 'a')
    {
      // Add User
      cout << "Enter username: ";
      cin >> username;
      cout << "Enter password: ";
      cin >> password;

      if(hTable.Add(username, password))
      {
        cout << "User " << username << " added."<<endl;;
      }
      else
      {
        cout << "*****Error: User already exists. Could not add user." << endl;;
      }
    }
    else if (c == 'r')
    {
      // Remove User
      cout << "Enter username: ";
      cin >> username;

		  if (hTable.Remove(username))
			{
        cout << "User " << username << " deleted." << endl;
      }
      else
      {
        cout << "*****Error: User not found.  Could not delete user" << endl;
      }
    }
    else if (c == 'c')
    {
      // Change User Password
      cout << "Enter username: ";
      cin >> username;
      cout << "Enter password: ";
      cin >> password;
      // password = getpass("");

      if (hTable.Change(username, password))
      {
        cout << "Password changed for user " << username << endl;
      }
      else
      {
        cout << "*****Error: Password incorrect.  Could not change user password" << endl;
      }
	  }
    else if (c == 'f')
    {
      // Find User
      cout << "Enter username: ";
      cin >> username;
      cout << "Enter password: ";
      cin >> password;

			if (hTable.Find(username, password))
  		{
        cout << "User '" << username << "' found." << endl;
      }
			else
  		{
        cout << "User '" << username << "' not found." << endl;
      }
    }
    else if (c == 'd')
    {
      // Dump HashTable
      hTable.Dump();
    }
    else if (c == 's')
    {
      // HashTable Size
      cout << "Size of hashtable: " << hTable.Size() << endl;
    }
    else if (c == 'w')
    {
      // Write to Password File
      cout << "Enter password file name to write to: ";
      cin >> filename;
      hTable.WriteFile(filename);
      cout << endl;
    }
    else
    {
      //Invalid Input.
      cout << "Invalid input" << endl;
    }

    Menu();
    cin >> c;
  }
}


int main()
{
  	start();
  	return(EXIT_SUCCESS);
}
