// Required

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <iostream>
#include <fstream>
#include <unistd.h>
#include <cstdlib>
#include <vector>
#include <list>
#include <string>
#include <utility>
#include <stdlib.h>
#include <sstream>
#include <iomanip>
#include <cctype>
#include <stdio.h>
#include <string.h>

using namespace std;

namespace cop4530
{
	static const unsigned int max_prime = 64997;

	class HashTable
	{
	public:
		HashTable();
    		HashTable(size_t n);
    		~HashTable();

    		int Size();
   		bool Add(string username, string password, bool encrypt = true);
		bool Remove(string username);
		bool Change(string username, string password);
		bool Find(string username, string password);
		bool LoadFile(string filename);
		bool WriteFile(string filename);
		void Dump();
	private:
    		unsigned int Index (const string &);
   		unsigned int hash_function   (const string& );
    		unsigned int prime_below(unsigned int);
    		void setPrimes(vector<unsigned int> &);
    		vector < list < pair <string, string> > > bucketVector;
  	};
}

#include "hashtable.cpp"

#endif
