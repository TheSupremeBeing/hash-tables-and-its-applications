spass.x: spass.cpp hashtable.cpp
	g++47 spass.cpp -o spass.x -lcrypt -std=c++11

run:
	./spass.x

test: spass.x
	./spass.x < test1

clean:
	rm spass.x
